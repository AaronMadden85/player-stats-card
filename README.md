# Player Stats Card

https://bitbucket.org/AaronMadden85/player-stats-card/src/master/
git clone git@bitbucket.org:AaronMadden85/player-stats-card.git

http://amadden.com/sites/player-stats-card/dist/

## Requirements

For development, you will only need Node.js and Gulp installed on your environement. The following versions were used in development of this component...

Node: v11.14.0
NPM: 6.7.0
Gulp:
    CLI version: 2.2.0
    Local version: 4.0.2

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v11.14.0

    $ npm --version
    6.7.0

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

#### Gulp installation

npm install --global gulp-cli

---

## Install

    $ git clone https://github.com/ORG/PROJECT.git
    $ cd PROJECT
    $ npm install

## Start, Watch and Build

    $ gulp

## Gulp dependencies
    gulp-sourcemaps
    gulp-sass
    gulp-concat
    gulp-uglify
    gulp-postcss
    autoprefixer
    cssnano
    gulp-replace

## Compiled files will be concatenated and placed in the 'dist' directory
