// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const replace = require('gulp-replace');

// File paths
const files = { 
    scssPath: 'resources/scss/**/*.scss',
    jsPath: 'resources/js/**/*.js',
    imgPath: 'resources/images/**/*.png',
    htmlPath: 'index.html',
    jsonPath: 'resources/data/player-stats.json'
}

// Sass task: compiles the style.scss file into style.css
function scssTask(){    
    return src(files.scssPath)
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass()) // compile SCSS to CSS
        .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('dist')
    ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src([
        'resources/js/utils/Utils.js',
        'resources/js/components/playerPickerComponent.js',
        'resources/js/components/logoShiftComponent.js',
        'resources/js/main.js',
        
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
        ])
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(dest('dist')
    );
}
function jsonTask(){
    return src(files.jsonPath)
        .pipe(dest('dist/resources/data/')
    ); // put final CSS in dist folder
}
function imgTask(){    
    return src(files.imgPath)
        .pipe(dest('dist/resources/images/')
    ); // put final CSS in dist folder
}
function htmlTask(){    
    return src(files.htmlPath)
        .pipe(replace('dist/', ''))
        .pipe(dest('dist/')
    ); // put final CSS in dist folder
}


// Cachebust
var cbString = new Date().getTime();
function cacheBustTask(){
    return src(['index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('.'));
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    watch([files.scssPath, files.jsPath, files.imgPath, files.htmlPath, files.jsonPath], 
        parallel(scssTask, jsTask, imgTask, htmlTask, jsonTask));    
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    parallel(scssTask, jsTask, imgTask, htmlTask, jsonTask), 
    cacheBustTask,
    watchTask
);