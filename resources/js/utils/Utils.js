var Utils = Utils || {};

Utils.loadData = function(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            dataObject = JSON.parse(this.responseText);
            callback(dataObject);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send(); 
};

//./resources/data/player-stats.json