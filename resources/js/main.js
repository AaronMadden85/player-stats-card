window.onload = function() {
    Utils.loadData('./resources/data/player-stats.json', dataLoaded);
    function dataLoaded(data) {
        playerPickerComponent.init(data, selectCallback);
        logoShiftComponent.init();
    }
    function selectCallback(data) {
        logoShiftComponent.changeImage(data[0], data[1]);
    }
}