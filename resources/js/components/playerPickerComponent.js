var playerPickerComponent = (function() {
    
    var dataObject,
        callback,
        selectBox,
        playerImage,
        playerName,
        playerPosition,
        statAppearences,
        statGoals,
        statAssists,
        statGPM,
        statPPM,
        player;

    function init(data, setCallback) {
        callback = setCallback;
        dataObject = data;
        selectBox = document.querySelectorAll('.select-box')[0];
        playerImage = document.querySelectorAll('.player-image')[0];
        playerName = document.querySelectorAll('.player-name')[0];
        playerPosition = document.querySelectorAll('.player-position')[0];
        statAppearences = document.querySelectorAll('.stat-appearences')[0];
        statGoals = document.querySelectorAll('.stat-goals')[0];
        statAssists = document.querySelectorAll('.stat-assists')[0];
        statGPM = document.querySelectorAll('.stat-GPM')[0];
        statPPM = document.querySelectorAll('.stat-PPM')[0];
        selectBox.selectedIndex = "0";
        addListeners();
    }

    function selectPlayer(index) {

        var stats = dataObject.players[index].stats;
        var sortObj = {};
        player = dataObject.players[index].player;
        sortObj.assist = 0;
        playerImage.src = player.image.imageUrl
        playerName.innerHTML = player.name.first+' '+player.name.last;
        playerPosition.innerHTML = player.info.positionInfo;

        for(var i=0; i<stats.length;i++) {
            if(stats[i].name == "appearances") sortObj.appear = stats[i].value;
            if(stats[i].name == "goals") sortObj.goals = stats[i].value;
            if(stats[i].name == "goal_assist") sortObj.assist = stats[i].value;
            if(stats[i].name == "fwd_pass") sortObj.fwdPass = stats[i].value;
            if(stats[i].name == "backward_pass") sortObj.bckPass = stats[i].value;
            if(stats[i].name == "mins_played") sortObj.mins = stats[i].value;
        }

        var roundStatsGPM =  sortObj.goals/sortObj.appear;
        var roundStatsMinute = (sortObj.bckPass+sortObj.fwdPass)/sortObj.mins;
        statGPM.innerHTML = roundStatsGPM.toFixed(2);
        statPPM.innerHTML = roundStatsMinute.toFixed(2);

        statAppearences.innerHTML = sortObj.appear;
        statGoals.innerHTML = sortObj.goals;
        statAssists.innerHTML = sortObj.assist;

    }

    function addListeners() {
        selectBox.addEventListener("change", changePlayer);
    }
    function changePlayer(evt) {
        selectPlayer(evt.target.value);
        callback([player.logoCoord.x, player.logoCoord.y]);
    }

    return {
      init: init,
    };
  })();