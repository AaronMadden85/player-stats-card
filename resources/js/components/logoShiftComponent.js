var logoShiftComponent = (function() {

    var div,
        frameWidth,
        frameHeight

    function init() {
        frameWidth = 80
        frameHeight = 80
        div = document.getElementById('logo-spritesheet');
    }

    function changeImage(x, y) {
        var xOffset = frameWidth * x,
            yOffset = frameHeight * y;
            div.style.backgroundPosition = -(xOffset) + "px " + -(yOffset) + "px";
    }

    return {
      init: init,
      changeImage:changeImage
    };
  })();